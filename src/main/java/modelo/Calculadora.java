/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Ignacio
 */
public class Calculadora {

    private float capital;
    private float tasaDeInteres;
    private int numeroAnios;
    private float resultadoInteresSimple;
    /**
     * @return the capital
     */
    public float getcapital() {
        return capital;
    }

    /**
     * @param capital the capital to set
     */
    public void setcapital(float capital) {
        this.capital = capital;
    }

    /**
     * @return the tasaDeInteres
     */
    public float getTasaDeInteres() {
        return tasaDeInteres;
    }

    /**
     * @param tasaDeInteres the tasaDeInteres to set
     */
    public void setTasaDeInteres(float tasaDeInteres) {
        this.tasaDeInteres = tasaDeInteres;
    }

    /**
     * @return the numeroAnios
     */
    public float getNumeroAnios() {
        return numeroAnios;
    }

    /**
     * @param numeroAnios the numeroAnios to set
     */
    public void setNumeroAnios(int numeroAnios) {
        this.numeroAnios = numeroAnios;
    }

    /**
     * @return the resultadoInteresSimple
     */
    public float getResultadoInteresSimple() {
        return resultadoInteresSimple;
    }

    /**
     * @param resultadoInteresSimple the resultadoInteresSimple to set
     */
    public void setResultadoInteresSimple(float resultadoInteresSimple) {
        this.resultadoInteresSimple = resultadoInteresSimple;
    }
    public void calcularInteresSimple(float capital, float tasaDeInteres,int numeroAnios){
        float tasa =(tasaDeInteres/100);
        this.resultadoInteresSimple = capital*tasa*numeroAnios;       
    }
    
    
}
